[Текущая версия (04.06.2015)](https://bitbucket.org/Namolem/diplom/downloads/DiplomOsipov_latest.pdf)

* Прописал номер приказа
* Добавил плакаты
* *Описание проблем" ->"Описание задач, требующих автоматизации"*
* *Убрал номер главы у "Заключение"*


[v0.6 (31.05.2015)](https://bitbucket.org/Namolem/diplom/downloads/DiplomOsipov_20150531_v06.pdf)

* **Доделал экономику**
* **Доделал БЖД**
* Добавил цель, основные задачи(Введение+Заключение аналит. части), основные требования к ПС(анал. часть)
* добавил номер страницы к аннотации
* орфография, пунктуация
* *убрал 1С из ТЗ*
* *уменьшил размер некоторых картинок*
* *отсортировал список литературы*


[v0.5 (27.05.2015)](https://bitbucket.org/Namolem/diplom/downloads/DiplomOsipov_20150527_v05.pdf)

* Добавил в аналитическую определения (4шт)
* Дополнил заключение аналитической части (1.4)
* *исправил размер текста нумерации страниц на 12pt*
* убрал точку в ссылке ("см. рис. 5.1. как пример..")
* *"рис." с маленькой буквы в тексте*
* *сделал рис. 3.1 меньше, чтобы он перепрыгнул на предыдущую страницу*
* *Исправил опечатки, точки, запятые*
* *Исправил таблицу, вылезающую за пределы листа в аналитической части*
* *Исправил опечатку в формуле БЖД*
* Удалил из БЖД раздел "Устойчивость процесса работы в чрезвычайных ситуациях"
* Добавил плакат
* Добавил диаграмму "Структура временных затрат"
* Добавил в экономику таблицу  "Значения коэффициента «c» для различных типов задач"
* **Доделал текст экономической части (не отдавал на проверку)**
* **Вернул "Введение"**
* *опечатки и неточности*
* заменил крестик умножения на точку


[v0.4 (17.05.2015)](https://bitbucket.org/Namolem/diplom/downloads/DiplomOsipov_20150517_v04.pdf)

* **Убрал "Введение" (перенес в Аналитическую часть).**
* **Добавил Аннотацию**
* Начало работы над экономической частью
* *Добавил пару записей в список литературы (исследовательская часть)*
* Перенес "объект и предмет исследования" из введения в выводы аналитической части
* *Убрал лишний отступ у заголовков второго уровня*
* *Исправил опечатки*
* **Добавил руководство пользователя**
* **Добавил руководство администратора**

[v0.3 (09.05.2015)](https://bitbucket.org/Namolem/diplom/downloads/DiplomOsipov_20150509_v03.pdf)

* **Дописал исследовательскую часть**

[v0.2 (03.05.2015)](https://bitbucket.org/Namolem/diplom/downloads/DiplomOsipov_20150503.pdf)

* **Добавил организационную часть**
* *Убрал заголовки 3-го уровня из содержания*
* *Заголовки 3 уровня жирным курсивом*
* *Заголовки второго уровня по центру, интервал после - 12pt*
* *Заголовки первого уровня 16pt*
* **Аннотация --> Введение. Переименовал и поместил после оглавления**
* **Дополнил Экспериментальную. Добавил скрины для "Показать на карте", описал результат нагрузочного тестирования**
* **Добавил текст в "Модель хранения данных"**
* *Первая диаграмма классов альбомной ориентацией, у второй уменьшил размер до 80%, чтобы влезал на страницу*
* *Переделал титульный лист: выравнивания по левому краю, "БРЯНСК 2015" капсом*

[v0.1 (26.04.2015)](https://bitbucket.org/Namolem/diplom/downloads/DiplomOsipov_20150426.pdf)

* Переключился на Times New Roman (XeLaTeX)
* Доработал титульный лист
* Добавил "Выводы"
* Дополнил "Техническое задание" (документация, порядок контроля и приемки, этапы разработки)
* Исправил неправильную ссылку (повторяющийся рисунок архитектуры системы)
* Добавил "Конструкторскую часть"
* Добавил "Заключение"
* Добавил "Экспериментальную часть"
* Добавил "Описание предметной области"
* Переименовал таблицу "Сравнение" -> "Получение итогового коэффициента"
* Добавил "Моделирование предметной области"
* Добавил "Сравнение программ-аналогов"
* Переделал титульный лист
* Техническая часть